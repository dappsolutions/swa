// GENERATED CODE - DO NOT MODIFY BY HAND
// Generated and consumed by 'simple_json' 

import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:swa/app/modules/login/storage/login.storage.dart';
import 'package:swa/app/modules/sidak/models/apd.models.dart';
import 'package:swa/app/modules/sidak/models/pekerjaan.models.dart';
import 'package:swa/app/modules/sidak/models/swa.models.dart';
import 'package:swa/app/modules/sidak/storage/image.storage.dart';

final _loginstorageMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => LoginStorage(
    username: mapper.applyDynamicFromJsonConverter(json['username'])!,
    password: mapper.applyDynamicFromJsonConverter(json['password'])!,
  ),
  (CustomJsonMapper mapper, LoginStorage instance) => <String, dynamic>{
    'username': mapper.applyDynamicFromInstanceConverter(instance.username),
    'password': mapper.applyDynamicFromInstanceConverter(instance.password),
  },
);


final _apdmodelMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => ApdModel(
    id: mapper.applyDynamicFromJsonConverter(json['id'])!,
    wp: mapper.applyDynamicFromJsonConverter(json['wp'])!,
    sop: mapper.applyDynamicFromJsonConverter(json['sop'])!,
    jsa: mapper.applyDynamicFromJsonConverter(json['jsa'])!,
    pengawas_k3: mapper.applyDynamicFromJsonConverter(json['pengawas_k3'])!,
    sertifikat_kom: mapper.applyDynamicFromJsonConverter(json['sertifikat_kom'])!,
    peralatan: mapper.applyDynamicFromJsonConverter(json['peralatan'])!,
    rambu_k3: mapper.applyDynamicFromJsonConverter(json['rambu_k3'])!,
    apd: mapper.applyDynamicFromJsonConverter(json['apd'])!,
  ),
  (CustomJsonMapper mapper, ApdModel instance) => <String, dynamic>{
    'id': mapper.applyDynamicFromInstanceConverter(instance.id),
    'wp': mapper.applyDynamicFromInstanceConverter(instance.wp),
    'sop': mapper.applyDynamicFromInstanceConverter(instance.sop),
    'jsa': mapper.applyDynamicFromInstanceConverter(instance.jsa),
    'pengawas_k3': mapper.applyDynamicFromInstanceConverter(instance.pengawas_k3),
    'sertifikat_kom': mapper.applyDynamicFromInstanceConverter(instance.sertifikat_kom),
    'peralatan': mapper.applyDynamicFromInstanceConverter(instance.peralatan),
    'rambu_k3': mapper.applyDynamicFromInstanceConverter(instance.rambu_k3),
    'apd': mapper.applyDynamicFromInstanceConverter(instance.apd),
  },
);


final _pekerjaanmodelMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => PekerjaanModel(
    id: mapper.applyDynamicFromJsonConverter(json['id'])!,
    jam_pekerjaan_dihentikan: mapper.applyDynamicFromJsonConverter(json['jam_pekerjaan_dihentikan'])!,
    jam_pekerjaan_dilanjutkan: mapper.applyDynamicFromJsonConverter(json['jam_pekerjaan_dilanjutkan'])!,
    ket_pekerjaan_dihentikan: mapper.applyDynamicFromJsonConverter(json['ket_pekerjaan_dihentikan'])!,
    temuan_1: mapper.applyDynamicFromJsonConverter(json['temuan_1'])!,
    jenis_temuan_1: mapper.applyDynamicFromJsonConverter(json['jenis_temuan_1'])!,
    temuan_2: mapper.applyDynamicFromJsonConverter(json['temuan_2'])!,
    jenis_temuan_2: mapper.applyDynamicFromJsonConverter(json['jenis_temuan_2'])!,
    rekomendasi: mapper.applyDynamicFromJsonConverter(json['rekomendasi'])!,
    nama_pengawas_k3: mapper.applyDynamicFromJsonConverter(json['nama_pengawas_k3'])!,
    nama_perusahaan: mapper.applyDynamicFromJsonConverter(json['nama_perusahaan'])!,
    ttd_k3: mapper.applyDynamicFromJsonConverter<dynamic>(json['ttd_k3'])!,
    ttd_swa: mapper.applyDynamicFromJsonConverter<dynamic>(json['ttd_swa'])!,
    ket_melanjutkan_pekerjaan: mapper.applyDynamicFromJsonConverter(json['ket_melanjutkan_pekerjaan'])!,
    status_temuan_1: mapper.applyDynamicFromJsonConverter(json['status_temuan_1'])!,
    status_temuan_2: mapper.applyDynamicFromJsonConverter(json['status_temuan_2'])!,
  ),
  (CustomJsonMapper mapper, PekerjaanModel instance) => <String, dynamic>{
    'id': mapper.applyDynamicFromInstanceConverter(instance.id),
    'jam_pekerjaan_dihentikan': mapper.applyDynamicFromInstanceConverter(instance.jam_pekerjaan_dihentikan),
    'jam_pekerjaan_dilanjutkan': mapper.applyDynamicFromInstanceConverter(instance.jam_pekerjaan_dilanjutkan),
    'ket_pekerjaan_dihentikan': mapper.applyDynamicFromInstanceConverter(instance.ket_pekerjaan_dihentikan),
    'temuan_1': mapper.applyDynamicFromInstanceConverter(instance.temuan_1),
    'jenis_temuan_1': mapper.applyDynamicFromInstanceConverter(instance.jenis_temuan_1),
    'temuan_2': mapper.applyDynamicFromInstanceConverter(instance.temuan_2),
    'jenis_temuan_2': mapper.applyDynamicFromInstanceConverter(instance.jenis_temuan_2),
    'rekomendasi': mapper.applyDynamicFromInstanceConverter(instance.rekomendasi),
    'nama_pengawas_k3': mapper.applyDynamicFromInstanceConverter(instance.nama_pengawas_k3),
    'nama_perusahaan': mapper.applyDynamicFromInstanceConverter(instance.nama_perusahaan),
    'ttd_k3': mapper.applyDynamicFromInstanceConverter<dynamic>(instance.ttd_k3),
    'ttd_swa': mapper.applyDynamicFromInstanceConverter<dynamic>(instance.ttd_swa),
    'ket_melanjutkan_pekerjaan': mapper.applyDynamicFromInstanceConverter(instance.ket_melanjutkan_pekerjaan),
    'status_temuan_1': mapper.applyDynamicFromInstanceConverter(instance.status_temuan_1),
    'status_temuan_2': mapper.applyDynamicFromInstanceConverter(instance.status_temuan_2),
  },
);


final _swamodelMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => SwaModel(
    id: mapper.applyDynamicFromJsonConverter(json['id'])!,
    nama_swa: mapper.applyDynamicFromJsonConverter(json['nama_swa'])!,
    email_swa: mapper.applyDynamicFromJsonConverter(json['email_swa'])!,
    lokasi_pekerjaan: mapper.applyDynamicFromJsonConverter(json['lokasi_pekerjaan'])!,
    uraian_pekerjaan: mapper.applyDynamicFromJsonConverter(json['uraian_pekerjaan'])!,
  ),
  (CustomJsonMapper mapper, SwaModel instance) => <String, dynamic>{
    'id': mapper.applyDynamicFromInstanceConverter(instance.id),
    'nama_swa': mapper.applyDynamicFromInstanceConverter(instance.nama_swa),
    'email_swa': mapper.applyDynamicFromInstanceConverter(instance.email_swa),
    'lokasi_pekerjaan': mapper.applyDynamicFromInstanceConverter(instance.lokasi_pekerjaan),
    'uraian_pekerjaan': mapper.applyDynamicFromInstanceConverter(instance.uraian_pekerjaan),
  },
);


final _imagestorageMapper = JsonObjectMapper(
  (CustomJsonMapper mapper, Map<String, dynamic> json) => ImageStorage(
    encodeImages: mapper.applyDynamicFromJsonConverter(json['encodeImages'])!,
  ),
  (CustomJsonMapper mapper, ImageStorage instance) => <String, dynamic>{
    'encodeImages': mapper.applyDynamicFromInstanceConverter(instance.encodeImages),
  },
);

void init() {
  JsonMapper.register(_loginstorageMapper);
  JsonMapper.register(_apdmodelMapper);
  JsonMapper.register(_pekerjaanmodelMapper);
  JsonMapper.register(_swamodelMapper);
  JsonMapper.register(_imagestorageMapper); 

  

  JsonMapper.registerListCast((value) => value?.cast<LoginStorage>().toList());
  JsonMapper.registerListCast((value) => value?.cast<ApdModel>().toList());
  JsonMapper.registerListCast((value) => value?.cast<PekerjaanModel>().toList());
  JsonMapper.registerListCast((value) => value?.cast<SwaModel>().toList());
  JsonMapper.registerListCast((value) => value?.cast<ImageStorage>().toList());
}
    