
class SessionConfig{
  static String user_id = "";
  static String user_email = "";
  static String user_name = "";

  static sessionSet(user, email, name){
    user_id = user;
    user_email = email;
    user_name = name;
  }

  static sessionClear(){
    user_id = "";
    user_email = "";
    user_name = "";
  }
}