import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
// import 'package:location/location.dart';
import 'package:swa/app/modules/lokasi/views/lokasimap.views.dart';
import 'package:swa/app/modules/sidak/views/sidak.views.dart';
import 'package:swa/app/modules/sidak/views/sidakdetail.views.dart';
import 'package:swa/app/modules/wpdetail/controllers/wpdetail.controllers.dart';
import 'package:swa/ui/Uicolor.dart';

class WpDetailViews extends StatefulWidget {
  String no_wp;
  String lokasi_pekerjaan;
  String uraian_pekerjaan;
  String pengawas_k3;
  String nama_vendor;
  bool VIEW_DETAIL;
  WpDetailViews({required this.no_wp, required this.lokasi_pekerjaan, required this.uraian_pekerjaan,
    required this.pengawas_k3, required this.nama_vendor,required this.VIEW_DETAIL});

  @override
  _WpDetailViewsState createState() => _WpDetailViewsState();
}

class _WpDetailViewsState extends State<WpDetailViews> {
  late WpDetailControllers controller;

  @override
  void initState() {
    controller = new WpDetailControllers();
    controller.getDetailDataWp(widget.no_wp, widget.VIEW_DETAIL);

    if(!widget.VIEW_DETAIL) {
      controller.polylinePoints = PolylinePoints();
      controller.setCurrentLocate(context);
    }
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DETAIL WP"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //DETAIL PEMOHON
                  Container(
                    color: Uicolor.hexToColor(Uicolor.grey_young),
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text(widget.no_wp, style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 20, fontWeight: FontWeight.bold),),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.person, color: Uicolor.hexToColor(Uicolor.green),size: 16,),
                          Text("Pemohon", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 16)),
                        ],
                      )
                  ),
                  Divider(
                    color: Uicolor.hexToColor(Uicolor.grey_young),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Nama Pemohon", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].nama_pemohon, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Perusahaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].perusahaan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("No Hp", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].no_hp, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("No Telp", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].no_telp, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Jabatan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].jabatan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Alamat", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].alamat, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  //DETAIL PEMOHON

                  //DETAIL WP
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 32),
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.assignment_turned_in, color: Uicolor.hexToColor(Uicolor.green),size: 16,),
                          Text("Permohonan Izin", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 16)),
                        ],
                      )
                  ),
                  Divider(
                    color: Uicolor.hexToColor(Uicolor.grey_young),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Tanggal WP", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].tanggal_wp, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Tanggal Awal Pelaksanaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].tgl_awal, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Tanggal Akhir Pelaksanaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].tgl_akhir, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Jabatan Penanggung Jawab Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].jabatan_penanggung_jawab, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Pengawas K3", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].pengawas_k3, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Jabatan Pengawas K3", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].jabatan_pengawas_k3, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Pengawas Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].pengawas_pekerjaan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Jabatan Pengawas Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].jabatan_pengawas_pekerjaan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  //DETAIL WP

                  //DETAIl PEKERJAAN
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 32),
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.work, color: Uicolor.hexToColor(Uicolor.green),size: 16,),
                          Text("Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 16)),
                        ],
                      )
                  ),
                  Divider(
                    color: Uicolor.hexToColor(Uicolor.grey_young),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Tempat", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].tempat, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Detail Tempat", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            if(widget.no_wp == 'SWA MANDIRI'){
                              return Text('-');
                            }
                            return Text(params.data[0].tempat_kerja, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Lokasi Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].lokasi_pekerjaan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Uraian Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].uraian_pekerjaan, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Keterangan Padam", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.data[0].keterangan_need_sistem == null ? '-' : params.data[0].keterangan_need_sistem, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Latitude", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.currentLatitudePos.toString(), style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                    alignment: Alignment.topLeft,
                    child: Text("Longitude", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                      alignment: Alignment.topRight,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return Text(params.currentLngitudePos.toString(), style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey), fontSize: 14));
                          }

                          if(params.loadingProses){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return Text("-");
                        },
                      )
                  ),
                  //DETAIl PEKERJAAN

                  Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: double.infinity,
                      // height: 50,
                      child: GetBuilder<WpDetailControllers>(
                        init: controller,
                        builder: (params){
                          if(params.data.length > 0){
                            return RaisedButton(
                              child: widget.VIEW_DETAIL ? Text("DETAIL SIDAK", style: TextStyle(color: Colors.white),) : Text("SIDAK", style: TextStyle(color: Colors.white),),
                              color: Colors.orangeAccent,
                              onPressed: (){
                                if(widget.VIEW_DETAIL){
                                  Get.to(SidakDetailViews(no_wp: widget.no_wp,));
                                }else{
                                  Navigator.of(context).pop();
                                  Get.to(SidakViews(
                                    no_wp: widget.no_wp,
                                    lat: controller.currentLatitudePos,
                                    lng: controller.currentLngitudePos,
                                    // lokasi_pekerjaan: widget.lokasi_pekerjaan,
                                    lokasi_pekerjaan: params.data[0].tempat_kerja,
                                    uraian_pekerjaan: widget.uraian_pekerjaan,
                                    pengawas_k3: widget.pengawas_k3,
                                    nama_vendor: widget.nama_vendor,
                                  ));
                                }
                              },
                              elevation: 3,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3.0),
                              ),
                            );
                          }

                          return Text('Tidak ada data');
                        },
                      )
                  ),
                ],
              )
          )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.map),
        backgroundColor: Colors.orangeAccent,
        onPressed: (){
          Get.to(LokasiMapViews(
            lat: controller.currentLatitudePos.toString(),
            lng: controller.currentLngitudePos.toString(),
          ));
        },
      ),
    );
  }
}