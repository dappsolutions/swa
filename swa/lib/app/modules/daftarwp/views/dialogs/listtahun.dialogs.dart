import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/daftarwp/controllers/listtahun.controllers.dart';
import 'package:swa/app/modules/daftarwp/models/upt.models.dart';
import 'package:swa/app/modules/grafik/views/grafik.views.dart';

class ListTahunDialogs extends StatefulWidget {

  @override
  _ListTahunDialogsState createState() => _ListTahunDialogsState();
}

class _ListTahunDialogsState extends State<ListTahunDialogs> {

  List<String> data = [];
  List<String> data_bulan = [];
  String default_tahun = DateTime.parse(DateTime.now().toString()).year.toString();
  String default_bulan = "Januari";

  late ListTahunControllers controllers;

  @override
  void initState() {
    controllers = new ListTahunControllers();

    var tgl = DateTime.now().toString();
    var dateParse = DateTime.parse(tgl);
    default_tahun = dateParse.year.toString();
    int year = int.parse(default_tahun);
    for(var i = 0; i < 3; i++){
      data.add(year.toString());
      year +=1;
    }

    data_bulan.add("Januari");
    data_bulan.add("Februari");
    data_bulan.add("Maret");
    data_bulan.add("April");
    data_bulan.add("Mei");
    data_bulan.add("Juni");
    data_bulan.add("Juli");
    data_bulan.add("Agustus");
    data_bulan.add("September");
    data_bulan.add("Oktober");
    data_bulan.add("November");
    data_bulan.add("Desember");

    // print("DATA ${data}");

    controllers.getListUpt();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
        height: 400,
        // width: 300,
        color: Colors.white,
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              child: Text("Pergi Ke Grafik SWA"),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
                height: 55,
                child: InputDecorator(
                  decoration:
                  InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: data
                            .map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: default_tahun,
                        hint: Text("Pilih Tahun"),
                        onChanged: (val) {
                          setState(() {
                            default_tahun = val.toString();
                          });
                        },
                      )
                  ),
                )
            ),
            SizedBox(
              height: 8,
            ),
            Container(
                height: 55,
                child: InputDecorator(
                  decoration:
                  InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: data_bulan
                            .map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: default_bulan,
                        hint: Text("Pilih Bulan"),
                        onChanged: (val) {
                          setState(() {
                            default_bulan = val.toString();
                          });
                        },
                      )
                  ),
                )
            ),
            SizedBox(
              height: 8,
            ),
            GetBuilder<ListTahunControllers>(
              init: controllers,
              builder: (params){
                  if(!params.isloading){
                    if(params.data.length > 0){
                      return Container(
                          height: 55,
                          child: InputDecorator(
                            decoration:
                            InputDecoration(border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  items: params.data
                                      .map((UptModel value) {
                                    return DropdownMenuItem<String>(
                                      value: value.id.toString(),
                                      child: Text(value.nama),
                                    );
                                  }).toList(),
                                  value: params.default_upt,
                                  // hint: Text("Pilih Tahun"),
                                  onChanged: (val) {
                                    params.default_upt = val.toString();
                                    controllers.update();
                                  },
                                )
                            ),
                          )
                      );
                    }

                    if(params.data.length == 0){
                        return Container(
                          child: Text("Tidak ada data upt"),
                        );
                    }
                  }

                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
              },
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: RaisedButton(
                child: Text("PROSES", style: TextStyle(color: Colors.white),),
                color: Colors.orangeAccent,
                onPressed: () async{
                  // Navigator.of(context).pop();
                  Navigator.pop(context);
                  Get.to(GrafikViews(tahun: default_tahun, month: default_bulan, upt: controllers.default_upt,));
                },
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.0),
                ),
              ),
            )
          ],
        )
    );
  }
}

