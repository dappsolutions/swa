import 'package:flutter/material.dart';
import 'package:swa/app/modules/daftarwp/controllers/daftarwp.controllers.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class GrafikTopTenViews extends StatelessWidget {
  DaftarWpController controller;
  List<charts.Series<dynamic, String>> seriesList;
  GrafikTopTenViews({required this.controller, required this.seriesList});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 300,
        padding: EdgeInsets.only(left: 16, right: 16),
        child: new charts.BarChart(
          this.seriesList,
          animate: true,
        )
        // child: Container()
        );
  }
}
