import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swa/app/modules/daftarsidak/views/daftarsidak.views.dart';
import 'package:swa/app/modules/daftarwp/controllers/daftarwp.controllers.dart';
import 'package:swa/app/modules/daftarwp/views/dialogs/listtahun.dialogs.dart';
import 'package:swa/app/modules/daftarwp/views/grafik.topten.views.dart';
import 'package:swa/app/modules/daftarwp/views/scanqr.views.dart';
import 'package:swa/app/modules/daftarwp/views/searchwp.views.dart';
import 'package:swa/app/modules/grafik/views/grafik.views.dart';
import 'package:swa/app/modules/login/views/login.views.dart';
import 'package:swa/app/modules/wpdetail/views/wpdetails.views.dart';
import 'package:swa/config/session.config.dart';
import 'package:swa/ui/Uicolor.dart';
import 'package:swa/app/modules/daftarwp/models/upt.models.dart';

class DaftarWpViews extends StatefulWidget {
  @override
  _DaftarWpViewsState createState() => _DaftarWpViewsState();
}

class _DaftarWpViewsState extends State<DaftarWpViews> {
  late DaftarWpController controller;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> data = [];
  List<String> data_bulan = [];
  String default_tahun =
      DateTime.parse(DateTime.now().toString()).year.toString();
  String default_bulan = "Januari";

  @override
  void initState() {
    controller = new DaftarWpController();
    controller.getAppVersion();
    controller.getListDaftarWp();
    default_bulan = controller.getDefaultBulan();
    controller.getGrafikTopTenSisak(default_tahun, default_bulan);

    var tgl = DateTime.now().toString();
    var dateParse = DateTime.parse(tgl);
    default_tahun = dateParse.year.toString();
    int year = int.parse(default_tahun);
    for (var i = 0; i < 3; i++) {
      data.add(year.toString());
      year += 1;
    }

    data_bulan.add("Januari");
    data_bulan.add("Februari");
    data_bulan.add("Maret");
    data_bulan.add("April");
    data_bulan.add("Mei");
    data_bulan.add("Juni");
    data_bulan.add("Juli");
    data_bulan.add("Agustus");
    data_bulan.add("September");
    data_bulan.add("Oktober");
    data_bulan.add("November");
    data_bulan.add("Desember");

    controller.getListUpt();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Uicolor.hexToColor(Uicolor.green),
                ),
                child: Text(
                  'APPLICATION',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              ListTile(
                title: Text('GRAFIK'),
                onTap: () {
                  Navigator.of(context).pop();
                  // Get.defaultDialog(title: "Pilihan Tahun", content: ListTahunDialogs(data: data,));
                  Get.bottomSheet(ListTahunDialogs());
                  // Get.to(GrafikViews());
                },
              ),
              ListTile(
                title: Text('INSPEKSI MANDIRI'),
                onTap: () async{
                  Navigator.of(context).pop();
                  Get.to(WpDetailViews(
                    no_wp: 'INSPEKSI MANDIRI',
                    // lokasi_pekerjaan: dataDetail.lokasi_pekerjaan,
                    lokasi_pekerjaan: '-',
                    uraian_pekerjaan: 'INSPEKSI MANDIRI',
                    pengawas_k3: '-',
                    nama_vendor: '-',
                    VIEW_DETAIL: false,
                  ));
                },
              ),
              ListTile(
                title: Text('KELUAR APLIKASI'),
                onTap: () async {
                  Navigator.of(context).pop();
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.clear();
                  Future.delayed(Duration(seconds: 2));
                  Get.offAll(LoginViews());
                  // Get.defaultDialog(title: "Pilihan Tahun", content: ListTahunDialogs(data: data,));

                  // Get.to(GrafikViews());
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text(
            "DAFTAR WP",
            style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),
          ),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.dehaze,
                  color: Uicolor.hexToColor(Uicolor.white),
                ),
                onPressed: () {
                  _scaffoldKey.currentState!.openDrawer();
                }),
            IconButton(
                icon: Icon(
                  Icons.notifications,
                  color: Uicolor.hexToColor(Uicolor.white),
                ),
                onPressed: () {
                  Get.to(DaftarSidakViews());
                }),
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                padding:
                    EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                child: Text(
                  "Data Working Permit",
                  style: TextStyle(fontSize: 24),
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                  child: GetBuilder<DaftarWpController>(
                init: controller,
                builder: (params) {
                  if (!params.loadingProses) {
                    if (params.data.length > 0) {
                      return ListView.builder(
                        itemCount: params.data.length,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemBuilder: (context, index) {
                          var data = params.data[index];
                          return Container(
                              height: MediaQuery.of(context).size.height * 0.12,
                              margin:
                                  EdgeInsets.only(left: 16, right: 18, top: 8),
                              child: GestureDetector(
                                child: Card(
                                  elevation: 0,
                                  color:
                                      Uicolor.hexToColor(Uicolor.green_smooth),
                                  child: Container(
                                    padding: EdgeInsets.all(16),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                            child: Icon(
                                          Icons.check_circle,
                                          color:
                                              Uicolor.hexToColor(Uicolor.green),
                                        )),
                                        Container(
                                          child: Text(data.no_wp),
                                        ),
                                        Container(
                                          child: Text(data.jam_sidak),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () => {
                                  // Get.to(WpDetailViews(no_wp: data.no_wp, VIEW_DETAIL: false,))
                                  Get.to(WpDetailViews(
                                    no_wp: data.no_wp,
                                    lokasi_pekerjaan: data.lokasi_pekerjaan,
                                    uraian_pekerjaan: data.uraian_pekerjaan,
                                    pengawas_k3: data.pengawas_k3,
                                    nama_vendor: data.nama_vendor,
                                    VIEW_DETAIL: false,
                                  ))
                                },
                              ));
                        },
                      );
                    }
                  }

                  if (params.data.length == 0) {
                    return Center(
                      child: Text("Tidak ada data ditemukan"),
                    );
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              )),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 24, right: 24),
                child: RaisedButton(
                  // child: Icon(Icons.refresh, color: Colors.white,),
                  child: Text(
                    "Tampilkan Lebih Banyak",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () {
                    controller.getListDaftarWp();
                  },
                  elevation: 3,
                  // shape:CircleBorder(),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Divider(),
              Container(
                  height: 88,
                  padding: EdgeInsets.all(16),
                  child: InputDecorator(
                    decoration: InputDecoration(border: OutlineInputBorder()),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                      items: data.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      value: default_tahun,
                      hint: Text("Pilih Tahun"),
                      onChanged: (val) {
                        setState(() {
                          default_tahun = val.toString();
                        });
                        controller.getGrafikTopTenSisak(
                            default_tahun, default_bulan);
                      },
                    )),
                  )),
              Container(
                  height: 80,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 8),
                  child: InputDecorator(
                    decoration: InputDecoration(border: OutlineInputBorder()),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                      items: data_bulan.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      value: default_bulan,
                      hint: Text("Pilih Bulan"),
                      onChanged: (val) {
                        setState(() {
                          default_bulan = val.toString();
                        });
                        controller.getGrafikTopTenSisak(
                            default_tahun, default_bulan);
                      },
                    )),
                  )),
              GetBuilder<DaftarWpController>(
                init: controller,
                builder: (params) {
                  if (!params.isloadingSelectedUpt) {
                    if (params.data.length > 0) {
                      return Container(
                          height: 82,
                          padding: EdgeInsets.only(
                              left: 16, right: 16, bottom: 16, top: 8),
                          child: InputDecorator(
                            decoration: InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                              items: params.dataUpt.map((UptModel value) {
                                return DropdownMenuItem<String>(
                                  value: value.id.toString(),
                                  child: Text(value.nama),
                                );
                              }).toList(),
                              value: params.default_upt,
                              // hint: Text("Pilih Tahun"),
                              onChanged: (val) {
                                params.default_upt = val.toString();
                                controller.update();

                                controller.getGrafikTopTenSisak(
                                    default_tahun, default_bulan);
                              },
                            )),
                          ));
                    }

                    if (params.data.length == 0) {
                      return Container(
                        child: Text("Tidak ada data upt"),
                      );
                    }
                  }

                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              ),
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text(
                  "Grafik Top 10",
                  style: TextStyle(fontSize: 24),
                  textAlign: TextAlign.left,
                ),
              ),
              GetBuilder<DaftarWpController>(
                init: controller,
                builder: (params) {
                  if (!params.loadingProsesGrafik) {
                    return GrafikTopTenViews(
                      controller: controller,
                      seriesList: params.seriesList,
                    );
                  }

                  return Container(
                    child: CircularProgressIndicator(),
                  );
                },
              )
            ],
          ),
        )),
        floatingActionButton: SpeedDial(
          backgroundColor: Colors.orangeAccent,
          children: [
            SpeedDialChild(
                backgroundColor: Colors.orangeAccent,
                child: Icon(Icons.refresh),
                onTap: () {
                  controller.getListDaftarWpAll();
                  controller.getGrafikTopTenSisak(default_tahun, default_bulan);
                }),
            SpeedDialChild(
                backgroundColor: Colors.orangeAccent,
                child: Icon(Icons.settings_overscan),
                onTap: () async {
                  // bool scan = await controller.scanQr();
                  // if (scan) {
                  //   if (controller.qrcoderesult == "") {
                  //     Get.snackbar("Informasi", "QR Tidak ditemukan",
                  //         backgroundColor: Colors.orangeAccent);
                  //   } else {
                      // await controller.getDetailDataWp(controller.qrcoderesult);
                      // var dataDetail = controller.dataDetail[0];
                  //     Get.to(WpDetailViews(
                  //       no_wp: controller.qrcoderesult,
                  //       // lokasi_pekerjaan: dataDetail.lokasi_pekerjaan,
                  //       lokasi_pekerjaan: dataDetail.tempat_kerja,
                  //       uraian_pekerjaan: dataDetail.uraian_pekerjaan,
                  //       pengawas_k3: dataDetail.pengawas_k3,
                  //       nama_vendor: dataDetail.perusahaan,
                  //       VIEW_DETAIL: false,
                  //     ));
                  //   }
                  // }                  
                  Get.to(ScanQrViews());
                }),
            SpeedDialChild(
                backgroundColor: Colors.orangeAccent,
                child: Icon(Icons.search),
                onTap: () {
                  Get.to(SearchWpViews());
                }),
          ],
        ));
  }
}
