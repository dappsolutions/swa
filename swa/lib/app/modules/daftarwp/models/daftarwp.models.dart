

class DaftarWpModel {
  late String id;
  late String no_wp;
  late String sidak_user;
  late String latitude;
  late String longitude;
  late String jam_sidak;
  late String lokasi_pekerjaan;
  late String uraian_pekerjaan;
  late String pengawas_k3;
  late String nama_vendor;
  DaftarWpModel({required this.id, required this.no_wp, required this.sidak_user,required this.latitude, required this.longitude, required this.jam_sidak,required this.lokasi_pekerjaan,required this.uraian_pekerjaan,required this.pengawas_k3,required this.nama_vendor});


  DaftarWpModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.no_wp = json['no_wp'];
    this.sidak_user = json['sidak_user'];
    this.latitude = json['latitude'];
    this.longitude = json['longitude'];
    this.jam_sidak = json['jam_sidak'];
    this.lokasi_pekerjaan = json['lokasi_pekerjaan'];
    this.uraian_pekerjaan = json['uraian_pekerjaan'];
    this.pengawas_k3 = json['pengawas_k3'];
    this.nama_vendor = json['nama_vendor'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no_wp'] = this.no_wp;
    data['sidak_user'] = this.sidak_user;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['jam_sidak'] = this.jam_sidak;
    data['lokasi_pekerjaan'] = this.lokasi_pekerjaan;
    data['uraian_pekerjaan'] = this.uraian_pekerjaan;
    data['pengawas_k3'] = this.pengawas_k3;
    data['nama_vendor'] = this.nama_vendor;

    return data;
  }
}