


class UptModel {
  late String id;
  late String nama;
  late String createddate;
  UptModel({required this.id, required this.nama, required this.createddate});


  UptModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.nama = json['nama'];
    this.createddate = json['createddate'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama'] = this.nama;
    data['createddate'] = this.createddate;
    return data;
  }
}