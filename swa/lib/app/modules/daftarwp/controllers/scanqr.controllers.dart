

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:swa/app/modules/wpdetail/models/detailwp.models.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';

class ScanQrControllers extends GetxController{
    bool loadingProses = false;
    List<DetailWpModel> dataDetail = [];

    Future<void> getDetailDataWp(String no_wp) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_DETAIL_DATA]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      this.dataDetail.add(DetailWpModel.fromJson(data['data']));
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }
}