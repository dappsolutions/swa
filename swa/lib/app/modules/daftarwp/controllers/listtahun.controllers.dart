

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/daftarwp/models/upt.models.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ListTahunControllers extends GetxController{
  bool isloading = false;
  List<UptModel> data = [];
  String default_upt = "2";

  void getListUpt() async{
    this.isloading = true;
    this.update();

    Map<String, dynamic> params = {};

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_GRAFIK][Routes.GET_LIST_UPT]), body: params);

    if(request.statusCode == 200){
      this.isloading = false;
      update();

      var dataUpt = json.decode(request.body);
      for(var val in dataUpt["data"]){
        this.data.add(UptModel.fromJson(val));
      }
      this.default_upt = this.data[this.data.length-1].id;
    }else{
      this.isloading = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

}