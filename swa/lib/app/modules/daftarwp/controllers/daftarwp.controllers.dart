import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swa/app/modules/daftarwp/models/daftarwp.models.dart';
import 'package:http/http.dart' as http;
import 'package:swa/app/modules/grafik/models/grafik.models.dart';
import 'package:swa/app/modules/version/models/version.model.dart';
import 'package:swa/app/modules/version/services/version.services.dart';
import 'package:swa/app/modules/wpdetail/models/detailwp.models.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'dart:convert';

import 'package:swa/config/routes.config.dart';
import 'package:swa/config/session.config.dart';
// import 'package:qrscan/qrscan.dart' as scanner;
import 'package:permission_handler/permission_handler.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:swa/app/modules/daftarwp/models/upt.models.dart';
import 'package:swa/config/url.config.dart';
import 'package:url_launcher/url_launcher.dart';

class DaftarWpController extends GetxController {
  List<DaftarWpModel> data = [];
  bool loadingProses = false;
  String qrcoderesult = "";
  List<DetailWpModel> dataDetail = [];
  late List<charts.Series<dynamic, String>> seriesList;
  List<GrafikModel> dataGrafik = [];

  bool loadingProsesGrafik = false;
  bool isloadingSelectedUpt = false;
  List<UptModel> dataUpt = [];
  String default_upt = "";
  List<VersionModels> dataVersion = [];
  String appVersion = '-';

  Future<void> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appVersion = packageInfo.version;
    String appName = packageInfo.appName;
    String buildNumber = packageInfo.buildNumber;
    String packageName = packageInfo.packageName;
    String buildSignature = packageInfo.buildSignature;
    this.appVersion = appVersion;
    this.update();

    this.getAppVersionApi(appVersion);
  }

  Future<void> getAppVersionApi(String appVersion) async {
    Map<String, dynamic> params = {};

    List<VersionModels> data =
        await VersionServices.getAppVersion(params, this.dataVersion);
    this.dataVersion = data;
    // print("DATA VERSION ${data[0].version}, ${appVersion}");
    if (data.isNotEmpty) {
      if (data[0].version != appVersion) {
        Get.dialog(
          AlertDialog(
            title: Text('Update Aplikasi'),
            content: Text(
                'Aplikasi Apar Versi ${appVersion} Sudah Usang, Silakan Update Aplikasi Sekarang'),
            actions: <Widget>[
              FlatButton(
                child: Text('Update Versi : ${data[0].version}'),
                onPressed: () {
                  this._launchInBrowser(UrlConfig.UPDATE_APP_URL);
                },
              ),
            ],
          ),
        );
      }
    }
    this.update();
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.user_email = pref.getString("email")!;
    SessionConfig.user_name = pref.getString("username")!;
    this.update();
  }

  Future<void> getListDaftarWp() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] =
        this.data.length > 0 ? this.data[this.data.length - 1].id : "";
    params["user_id"] = SessionConfig.user_id;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATA]),
        body: params);

    // print("DATA WP LIST ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      for (var val in data["data"]) {
        this.data.add(DaftarWpModel.fromJson(val));
      }
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  void getListDaftarWpAll() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GETDATA]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      this.data.clear();

      var data = json.decode(request.body);
      for (var val in data["data"]) {
        this.data.add(DaftarWpModel.fromJson(val));
      }
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  Future<bool> scanQr() async {
    await Permission.camera.request();
    // String barcode = await scanner.scan();
    // if (barcode == null) {
    //   // print('nothing return.');
    //   this.qrcoderesult = "";
    // } else {
    //   this.qrcoderesult = barcode;
    // }
    this.update();

    return true;
  }

  Future<void> getDetailDataWp(String no_wp) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;

    var request = await http.post(
        Uri.parse(
            Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_DETAIL_DATA]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      this.dataDetail.add(DetailWpModel.fromJson(data['data']));
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }

  void getGrafikTopTenSisak(String default_tahun, String default_bulan) async {
    this.loadingProsesGrafik = true;
    this.update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["tahun"] = default_tahun;
    params["bulan"] = default_bulan;
    params["upt"] = this.default_upt;

    // print("DATA FILTER ${params}");

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_GRAFIK][Routes.GETDATATOPTEN]),
        body: params);

    // print("RESULT ${request.body}");
    if (request.statusCode == 200) {
      this.loadingProsesGrafik = false;
      this.dataGrafik.clear();
      update();

      var dataGrafik = json.decode(request.body);
      for (var val in dataGrafik["data"]) {
        this.dataGrafik.add(GrafikModel.fromJson(val));
      }
    } else {
      this.loadingProsesGrafik = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    List<charts.Series<GrafikModel, String>> datagrafik = [];
    datagrafik = [
      new charts.Series<GrafikModel, String>(
        id: 'Sidak',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (GrafikModel sales, _) => sales.month,
        measureFn: (GrafikModel sales, _) => int.parse(sales.total),
        data: this.dataGrafik,
      )
    ];

    this.loadingProsesGrafik = false;
    this.seriesList = datagrafik;
    this.update();
  }

  String getDefaultBulan() {
    String month = DateTime.parse(DateTime.now().toString()).month.toString();
    String defaultMonth = "Januari";
    switch (month) {
      case "1":
        defaultMonth = "Januari";
        break;
      case "2":
        defaultMonth = "Februari";
        break;
      case "3":
        defaultMonth = "Maret";
        break;
      case "4":
        defaultMonth = "April";
        break;
      case "5":
        defaultMonth = "Mei";
        break;
      case "6":
        defaultMonth = "Juni";
        break;
      case "7":
        defaultMonth = "Juli";
        break;
      case "8":
        defaultMonth = "Agustus";
        break;
      case "9":
        defaultMonth = "September";
        break;
      case "10":
        defaultMonth = "Oktober";
        break;
      case "11":
        defaultMonth = "November";
        break;
      case "12":
        defaultMonth = "Desember";
        break;
    }

    return defaultMonth;
  }

  void getListUpt() async {
    this.isloadingSelectedUpt = true;
    this.update();

    Map<String, dynamic> params = {};

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_GRAFIK][Routes.GET_LIST_UPT]),
        body: params);

    if (request.statusCode == 200) {
      this.isloadingSelectedUpt = false;
      update();

      var dataUpt = json.decode(request.body);
      for (var val in dataUpt["data"]) {
        if (val['id'].toString() != '2') {
          this.dataUpt.add(UptModel.fromJson(val));
        }
      }
      this.default_upt = this.dataUpt[this.dataUpt.length - 1].id;
    } else {
      this.isloadingSelectedUpt = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();
  }
}
