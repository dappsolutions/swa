

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainControllers extends GetxController{
  bool hasLogin = false;

  void checkSudahLogin() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.hasLogin = prefs.getString("user_id") != null ? true : false;
    this.update();
  }
}