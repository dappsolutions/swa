import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/daftarsidak/controllers/daftarsidak.controllers.dart';
import 'package:swa/app/modules/daftarsidak/views/searchsidak.views.dart';
import 'package:swa/app/modules/wpdetail/views/wpdetails.views.dart';
import 'package:swa/ui/Uicolor.dart';

class DaftarSidakViews extends StatefulWidget {
  @override
  _DaftarSidakViewsState createState() => _DaftarSidakViewsState();
}

class _DaftarSidakViewsState extends State<DaftarSidakViews> {
  late DaftarSidakControllers controller;

  @override
  void initState() {
    controller = new DaftarSidakControllers();
    controller.getListWpSudahSidak();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("DAFTAR SIDAK WP", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    child: GetBuilder<DaftarSidakControllers>(
                      init: controller,
                      builder: (params){
                        if(!params.loadingProses){
                          if(params.data.length > 0){
                            return ListView.builder(
                              itemCount: params.data.length,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemBuilder: (context, index){
                                var data = params.data[index];
                                return Container(
                                    height: MediaQuery.of(context).size.height * 0.12,
                                    margin: EdgeInsets.only(left: 16, right: 18, top: 8),
                                    child: GestureDetector(
                                      child: Card(
                                        elevation: 0,
                                        color: Uicolor.hexToColor(Uicolor.green_smooth),
                                        child: Container(
                                          padding: EdgeInsets.all(16),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                  child: Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),)
                                              ),
                                              Container(
                                                child: Text(data.no_wp),
                                              ),
                                              Container(
                                                child: Text(data.jam_sidak),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onTap: ()=>{
                                        Get.to(WpDetailViews(
                                          no_wp: data.no_wp,
                                          lokasi_pekerjaan: data.lokasi_pekerjaan,
                                          uraian_pekerjaan: data.uraian_pekerjaan,
                                          pengawas_k3: data.pengawas_k3,
                                          nama_vendor: data.nama_vendor,
                                          VIEW_DETAIL: true,)
                                        )
                                      },
                                    )
                                );
                              },
                            );
                          }
                        }

                        if(params.data.length == 0){
                          return Center(
                            child: Text("Tidak ada data ditemukan"),
                          );
                        }

                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    )
                ),

                Container(
                  child: RaisedButton(
                    child: Icon(Icons.refresh, color: Colors.white,),
                    color: Uicolor.hexToColor(Uicolor.green),
                    onPressed: (){
                      controller.getListWpSudahSidak();
                    },
                    elevation: 3,
                    shape:CircleBorder(),
                  ),
                )
              ],
            )
          ),
        ),
        floatingActionButton: SpeedDial(
          backgroundColor: Colors.orangeAccent,
          children: [
            // SpeedDialChild(
            //     backgroundColor: Colors.orangeAccent,
            //     child: Icon(Icons.settings_overscan),
            //     onTap: (){
            //
            //     }
            // ),
            SpeedDialChild(
                backgroundColor: Colors.orangeAccent,
                child: Icon(Icons.search),
                onTap: (){
                  Get.to(SearchSidakViews());
                }
            ),
          ],
        )
    );
  }
}
