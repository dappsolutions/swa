

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swa/app/modules/daftarwp/models/daftarwp.models.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';
import 'package:swa/config/session.config.dart';
import 'package:http/http.dart' as http;
import 'package:swa/config/api.config.dart';
import 'dart:convert';

class SearchSidakControllers extends GetxController{
  List<DaftarWpModel> data = [];
  bool loadingProses = false;


  Future<void> getUserIdSession() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.user_email = pref.getString("email")!;
    SessionConfig.user_name = pref.getString("username")!;
    this.update();
  }

  Future<void> getListWpSudahSidak(String keyword) async{
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["keyword"] = keyword;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_PERMIT][Routes.GET_LIST_SIDAK_SEARCH]), body: params);

    if(request.statusCode == 200){
      this.loadingProses = false;
      update();

      var data = json.decode(request.body);
      for(var val in data["data"]){
        this.data.add(DaftarWpModel.fromJson(val));
      }

    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }
}