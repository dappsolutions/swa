


import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class PekerjaanModel {
  late String id;
  late String jam_pekerjaan_dihentikan;
  late String jam_pekerjaan_dilanjutkan;
  late String ket_pekerjaan_dihentikan;
  late String temuan_1;
  late String jenis_temuan_1;
  late String temuan_2;
  late String jenis_temuan_2;
  late String rekomendasi;
  late String nama_pengawas_k3;
  late String nama_perusahaan;
  late var ttd_k3;
  late var ttd_swa;
  late String ket_melanjutkan_pekerjaan;
  late String status_temuan_1;
  late String status_temuan_2;

  PekerjaanModel({
    required this.id,
    required this.jam_pekerjaan_dihentikan,
    required this.jam_pekerjaan_dilanjutkan,
    required this.ket_pekerjaan_dihentikan,
    required this.temuan_1,
    required this.jenis_temuan_1,
    required this.temuan_2,
    required this.jenis_temuan_2,
    required this.rekomendasi,
    required this.nama_pengawas_k3,
    required this.nama_perusahaan,
    required this.ttd_k3,
    required this.ttd_swa,
    required this.ket_melanjutkan_pekerjaan,
    required this.status_temuan_1,
    required this.status_temuan_2,
  });


  PekerjaanModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.jam_pekerjaan_dihentikan = json['jam_pekerjaan_dihentikan'];
    this.jam_pekerjaan_dilanjutkan = json['jam_pekerjaan_dilanjutkan'];
    this.ket_pekerjaan_dihentikan = json['ket_pekerjaan_dihentikan'];
    this.temuan_1 = json['temuan_1'];
    this.jenis_temuan_1 = json['jenis_temuan_1'];
    this.temuan_2 = json['temuan_2'];
    this.jenis_temuan_2 = json['jenis_temuan_2'];
    this.rekomendasi = json['rekomendasi'];
    this.nama_pengawas_k3 = json['nama_pengawas_k3'];
    this.nama_perusahaan = json['nama_perusahaan'];
    this.ttd_k3 = json['ttd_k3'];
    this.ttd_swa = json['ttd_swa'];
    this.ket_melanjutkan_pekerjaan = json['ket_melanjutkan_pekerjaan'];
    this.status_temuan_1 = json['status_temuan_1'];
    this.status_temuan_2 = json['status_temuan_2'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['jam_pekerjaan_dihentikan'] = this.jam_pekerjaan_dihentikan.toString();
    data['jam_pekerjaan_dilanjutkan'] = this.jam_pekerjaan_dilanjutkan.toString();
    data['ket_pekerjaan_dihentikan'] = this.ket_pekerjaan_dihentikan.toString();
    data['temuan_1'] = this.temuan_1.toString();
    data['jenis_temuan_1'] = this.jenis_temuan_1.toString();
    data['temuan_2'] = this.temuan_2.toString();
    data['jenis_temuan_2'] = this.jenis_temuan_2.toString();
    data['rekomendasi'] = this.rekomendasi.toString();
    data['nama_pengawas_k3'] = this.nama_pengawas_k3.toString();
    data['nama_perusahaan'] = this.nama_perusahaan.toString();
    data['ttd_k3'] = this.ttd_k3.toString();
    data['ttd_swa'] = this.ttd_swa.toString();
    data['ket_melanjutkan_pekerjaan'] = this.ket_melanjutkan_pekerjaan.toString();
    data['status_temuan_1'] = this.status_temuan_1.toString();
    data['status_temuan_2'] = this.status_temuan_2.toString();
    return data;
  }
}