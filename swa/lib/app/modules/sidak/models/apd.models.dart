

import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class ApdModel {
  late String id;
  late String wp;
  late String sop;
  late String jsa;
  late String pengawas_k3;
  late String sertifikat_kom;
  late String peralatan;
  late String rambu_k3;
  late String apd;

  ApdModel({
    required this.id,
    required this.wp,
    required this.sop,
    required this.jsa,
    required this.pengawas_k3,
    required this.sertifikat_kom,
    required this.peralatan,
    required this.rambu_k3,
    required this.apd,
  });


   ApdModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.wp = json['wp'];
    this.sop = json['sop'];
    this.jsa = json['jsa'];
    this.pengawas_k3 = json['pengawas_k3'];
    this.sertifikat_kom = json['sertifikat_kom'].toString();
    this.peralatan = json['peralatan'].toString();
    this.rambu_k3 = json['rambu_k3'].toString();
    this.apd = json['apd'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['wp'] = this.wp.toString();
    data['sop'] = this.sop.toString();
    data['jsa'] = this.jsa.toString();
    data['pengawas_k3'] = this.pengawas_k3.toString();
    data['sertifikat_kom'] = this.sertifikat_kom.toString();
    data['peralatan'] = this.peralatan.toString();
    data['rambu_k3'] = this.rambu_k3.toString();
    data['apd'] = this.apd.toString();
    return data;
  }
}