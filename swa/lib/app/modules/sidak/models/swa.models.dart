


import 'dart:io';

import 'package:simple_json_mapper/simple_json_mapper.dart';

@JObj()
class SwaModel {
  late String id;
  late String nama_swa;
  late String email_swa;
  late String lokasi_pekerjaan;
  late String uraian_pekerjaan;

  SwaModel({
    required this.id,
    required this.nama_swa,
    required this.email_swa,
    required this.lokasi_pekerjaan,
    required this.uraian_pekerjaan,
  });


  SwaModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.nama_swa = json['nama_swa'];
    this.email_swa = json['email_swa'];
    this.lokasi_pekerjaan = json['lokasi_pekerjaan'];
    this.uraian_pekerjaan = json['uraian_pekerjaan'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['nama_swa'] = this.nama_swa.toString();
    data['email_swa'] = this.email_swa.toString();
    data['lokasi_pekerjaan'] = this.lokasi_pekerjaan.toString();
    data['uraian_pekerjaan'] = this.uraian_pekerjaan.toString();
    return data;
  }
}