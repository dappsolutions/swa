import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:swa/app/modules/response/models/message.models.dart';
import 'package:swa/app/modules/sidak/models/apd.models.dart';
import 'package:swa/app/modules/sidak/models/image.models.dart';
import 'package:swa/app/modules/sidak/models/pekerjaan.models.dart';
import 'package:swa/app/modules/sidak/models/swa.models.dart';
import 'package:swa/app/modules/sidak/storage/image.storage.dart';
import 'package:signature/signature.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';
import 'package:swa/config/session.config.dart';
import 'package:swa/mapper.g.dart' as mapper;

class SidakControllers extends GetxController {
  List<ImageModel> data_img = [];
  List<ImageStorage> data_img_store = [];

  //Data SWA
  String nama_pemegang = "";
  String email_pemegang = "";
  String lokasi_pekerjaan = "";
  String uraian_pekerjaan = "";
  String pengawas_k3 = "";
  String nama_vendor = "";
  //Data SWA

  //kelengkapan
  bool CHECKED_WP = false;
  bool CHECKED_SOP = false;
  bool CHECKED_JSA = false;
  bool CHECKED_PENGAWASK3 = false;
  bool CHECKED_PEKERJA_SER = false;
  bool CHECKED_PERALATAN = false;
  bool CHECKED_RAMBUK3 = false;
  bool CHECKED_MASKER = false;
  bool CHECKED_APD = false;
  //kelengkapan

  //lat lng
  double lat = 0;
  double lng = 0;

  //pekerjaan
  String jam_pelaksanaan_kerja = "";
  String jam_lanjut_pelaksanaan_kerja = "";
  String ket_dihentikan_kerja = "";
  String temuan_1 = "";
  String jenis_temuan_1 = "";
  String temuan_2 = "";
  String jenis_temuan_2 = "";
  String rekomendasi = "";
  String ket_melanjutkan_pekerjaan = "";
  String status_temuan_1 = "";
  String status_temuan_2 = "";
  String nama_pengawas_k3 = "";
  String nama_perusahaan = "";

  String jenis_status_temuan_1 = '';
  String jenis_status_temuan_2 = '';

  var img_ttd_k3 = "";
  var img_ttd_swa = "";
  //pekerjaan

  late ApdModel dataApd;
  late SwaModel dataSwa;
  late PekerjaanModel dataPekerjaa;

  bool loadingProses = false;

  String no_transaksi = "";

  void getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.user_email = pref.getString("email")!;
    SessionConfig.user_name = pref.getString("username")!;    
    this.update();
  }

  void setInitValueForm(
      String lok, String uraian, String pengawas_k3, String nama_vendor) async {
    this.nama_pemegang = SessionConfig.user_name;
    this.email_pemegang = SessionConfig.user_email;
    this.lokasi_pekerjaan = lok;
    this.uraian_pekerjaan = uraian;
    this.nama_pengawas_k3 = pengawas_k3;
    this.nama_perusahaan = nama_vendor;
    setSwaData();

    // print('NAMA VENDOR ${nama_vendor}');
    // print('PENGAWS K3 ${pengawas_k3}');
  }

  void setChecked(String jenis) {
    switch (jenis) {
      case "wp":
        if (this.CHECKED_WP) {
          this.CHECKED_WP = false;
        } else {
          this.CHECKED_WP = true;
        }
        break;
      case "sop":
        if (this.CHECKED_SOP) {
          this.CHECKED_SOP = false;
        } else {
          this.CHECKED_SOP = true;
        }
        break;
      case "jsa":
        if (this.CHECKED_JSA) {
          this.CHECKED_JSA = false;
        } else {
          this.CHECKED_JSA = true;
        }
        break;
      case "pengawas_k3":
        if (this.CHECKED_PENGAWASK3) {
          this.CHECKED_PENGAWASK3 = false;
        } else {
          this.CHECKED_PENGAWASK3 = true;
        }
        break;
      case "sertifikat_kom":
        if (this.CHECKED_PEKERJA_SER) {
          this.CHECKED_PEKERJA_SER = false;
        } else {
          this.CHECKED_PEKERJA_SER = true;
        }
        break;
      case "peralatan":
        if (this.CHECKED_PERALATAN) {
          this.CHECKED_PERALATAN = false;
        } else {
          this.CHECKED_PERALATAN = true;
        }
        break;
      case "rambu_k3":
        if (this.CHECKED_RAMBUK3) {
          this.CHECKED_RAMBUK3 = false;
        } else {
          this.CHECKED_RAMBUK3 = true;
        }
        break;
      case "apd":
        if (this.CHECKED_APD) {
          this.CHECKED_APD = false;
        } else {
          this.CHECKED_APD = true;
        }
        break;
    }

    this.dataApd = new ApdModel(
      id: "",
      wp: this.CHECKED_WP ? "yes" : "",
      sop: this.CHECKED_SOP ? "yes" : "",
      jsa: this.CHECKED_JSA ? "yes" : "",
      pengawas_k3: this.CHECKED_PENGAWASK3 ? "yes" : "",
      sertifikat_kom: this.CHECKED_PEKERJA_SER ? "yes" : "",
      peralatan: this.CHECKED_PERALATAN ? "yes" : "",
      rambu_k3: this.CHECKED_RAMBUK3 ? "yes" : "",
      apd: this.CHECKED_APD ? "yes" : "",
    );

    // print("DATA APD ${this.dataApd}");
    this.update();
  }

  void setSwaData() {
    this.dataSwa = new SwaModel(
      id: "",
      nama_swa: this.nama_pemegang,
      email_swa: this.email_pemegang,
      lokasi_pekerjaan: this.lokasi_pekerjaan,
      uraian_pekerjaan: this.uraian_pekerjaan,
    );
    this.update();
  }

  void setPekerjaanData() {
    this.dataPekerjaa = new PekerjaanModel(
      id: "",
      jam_pekerjaan_dihentikan: this.jam_pelaksanaan_kerja,
      jam_pekerjaan_dilanjutkan: this.jam_lanjut_pelaksanaan_kerja,
      ket_pekerjaan_dihentikan: this.ket_dihentikan_kerja,
      temuan_1: this.temuan_1,
      jenis_temuan_1: this.jenis_temuan_1,
      temuan_2: this.temuan_2,
      jenis_temuan_2: this.jenis_temuan_2,
      rekomendasi: this.rekomendasi,
      nama_pengawas_k3: this.nama_pengawas_k3,
      nama_perusahaan: this.nama_perusahaan,
      ttd_k3: this.img_ttd_k3,
      ttd_swa: this.img_ttd_swa,
      ket_melanjutkan_pekerjaan: this.ket_melanjutkan_pekerjaan,
      status_temuan_1: this.status_temuan_1,
      status_temuan_2: this.status_temuan_2,
    );
    this.update();
  }

  // void pickImg() async {
  //   // this.data_img.add("TES");
  //   // this.update();
  //   final pickedFile =
  //       // await ImagePicker().getImage(source: ImageSource.gallery);
  //       await ImagePicker().getImage(source: ImageSource.camera);
  //   File img_from = File(pickedFile.path);

  //   final dir = await path_provider.getTemporaryDirectory();
  //   List<String> dataPath = pickedFile.path.split('/');

  //   String temporaryName = dataPath[dataPath.length - 1].replaceAll('.jpg', '');
  //   temporaryName = temporaryName.replaceAll('.png', '');
  //   temporaryName = temporaryName.replaceAll('.jpeg', '');
  //   temporaryName = temporaryName + "-temp.jpg";
  //   final targetPath = dir.absolute.path + "/${temporaryName}";

  //   img_from = await FlutterImageCompress.compressAndGetFile(
  //     pickedFile.path,
  //     targetPath,
  //     quality: 50,
  //     minWidth: 450,
  //     minHeight: 450,
  //     // rotate: 90,
  //   );

  //   final bytes = await img_from.readAsBytes();
  //   this
  //       .data_img
  //       .add(ImageModel(file: img_from, encodeImages: base64.encode(bytes)));
  //   this.data_img_store.add(ImageStorage(encodeImages: base64.encode(bytes)));

  //   this.update();
  // }

   void pickImg(bool camera) async {
    // this.data_img.add("TES");
    // this.update();
    final pickedFile =
        // await ImagePicker().getImage(source: ImageSource.gallery);
        camera
            ? await ImagePicker().getImage(source: ImageSource.camera)
            : await ImagePicker().getImage(source: ImageSource.gallery);    
    if (pickedFile != null) {
      File img_from = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();
      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = temporaryName + "-temp.jpg";
      final targetPath = dir.absolute.path + "/${temporaryName}";

      img_from = (await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 20,
        minWidth: 300,
        minHeight: 300,
        // rotate: 90,
      ))!;

      final bytes = await img_from.readAsBytes();
      this
          .data_img
          .add(ImageModel(file: img_from, encodeImages: base64.encode(bytes)));
      this.data_img_store.add(ImageStorage(encodeImages: base64.encode(bytes)));
    }

    this.update();
  }

  void clearDataImg() {
    this.data_img = [];
    this.data_img_store = [];
    this.update();
  }

  void clearDataImgAt(int item) {
    this.data_img.removeAt(item);
    this.data_img_store.removeAt(item);
    this.update();
  }

  simpanGambar() {
    // mapper.init();
    // final jsonStr = JsonMapper.serialize(this.data_img_store);
    print('Serialized JSON:');
    // print(jsonStr);
  }

  void getDetailSidak(String no_wp) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_wp"] = no_wp;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_DATA]),
        body: params);

    print("DATA DETAIL ${request.body.toString()}");
    // return;
    try {      
      if (request.statusCode == 200) {
        this.loadingProses = false;
        update();

        var data = json.decode(request.body);
        this.dataSwa = SwaModel.fromJson(data['data_swa']);
        this.dataPekerjaa = PekerjaanModel.fromJson(data['data_pekerjaan']);
        this.dataApd = ApdModel.fromJson(data['data_apd']);
      } else {
        this.loadingProses = false;
        update();

        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    } catch (e) {
    }

    this.update();
  }

  Widget widgetImgPick(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(16),
        child: DottedBorder(
          dashPattern: [8, 4],
          strokeWidth: 2,
          color: Colors.grey,
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 80),
                  child: Center(
                      child: GestureDetector(
                    child: Icon(
                      Icons.photo_camera,
                      color: Colors.grey,
                    ),
                    onTap: () {
                      this.pickImg(true);
                    },
                  )),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  margin: EdgeInsets.only(top: 6),
                  child: Center(
                    child: Text("Ambil Gambar"),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget widgetImgSignature(
      BuildContext context, SignatureController controller) {
    return Container(
        padding: EdgeInsets.all(16),
        height: 200,
        child: DottedBorder(
          dashPattern: [8, 4],
          strokeWidth: 2,
          color: Colors.grey,
          child: Container(
              width: MediaQuery.of(context).size.width,
              child: Container(
                height: 200,
                child: Signature(
                  controller: controller,
                  height: 200,
                  backgroundColor: Colors.white,
                ),
                //O
              )),
        ));
  }

  void setUnsafeCondition(int status, int jenis) {
    switch (jenis) {
      case 1:
        this.jenis_status_temuan_1 = "";
        this.status_temuan_1 = status.toString();
        if (status == 0) {
          this.jenis_status_temuan_1 = "UC";
        }
        if (status == 1) {
          this.jenis_status_temuan_1 = "UA";
        }
        break;
      case 2:
        this.jenis_status_temuan_2 = "";
        this.status_temuan_2 = status.toString();
        if (status == 0) {
          this.jenis_status_temuan_2 = "UC";
        }
        if (status == 1) {
          this.jenis_status_temuan_2 = "UA";
        }
        break;
    }

    update();
  }

  Future<int> simpanAll(String no_wp, double lat, double lng) async {
    int result = 0;
    this.loadingProses = true;
    this.update();

    mapper.init();
    final jsonStr = JsonMapper.serialize(this.data_img_store);

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_wp"] = no_wp;
    params["lat"] = lat.toString();
    params["lng"] = lng.toString();
    params["data_image"] = jsonStr;
    params["data_apd"] = JsonMapper.serialize(this.dataApd);
    params["data_swa"] = JsonMapper.serialize(this.dataSwa);
    params["data_pekerjaan"] = JsonMapper.serialize(this.dataPekerjaa);
    // print("PARAMS ${params}");

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANALL]),
        body: params);

    debugPrint("RESPONSE "+request.body);
  
    try {      
      if (request.statusCode == 200) {
        this.loadingProses = false;
        update();
        var data = json.decode(request.body);
        MessageModel respMessage = MessageModel.fromJson(data);
        if (respMessage.is_valid == "1") {
          this.no_transaksi = data['no_trans'];
          this.update();
          result = 1;
        } else {
          Get.snackbar("Informasi", "Gagal Simpan Data",
              backgroundColor: Colors.redAccent, colorText: Colors.white);
          result = 0;
        }
      } else {
        this.loadingProses = false;
        update();

        if (request.statusCode == 500) {
          Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
              backgroundColor: Colors.redAccent, colorText: Colors.white);
        } else {
          Get.snackbar("Informasi", "Gagal Memuat Data",
              backgroundColor: Colors.redAccent, colorText: Colors.white);
        }
        result = 0;
      }
    } catch (e) {
      debugPrint("ERROR "+e.toString());
    }

    return result;
  }

}
