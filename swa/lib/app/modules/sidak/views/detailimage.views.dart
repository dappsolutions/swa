import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/sidak/controllers/image.controllers.dart';
import 'package:swa/ui/Uicolor.dart';

class DetailImageViews extends StatefulWidget {
  String no_wp;
  DetailImageViews({required this.no_wp});

  @override
  _DetailImageViewsState createState() => _DetailImageViewsState();
}

class _DetailImageViewsState extends State<DetailImageViews> {
  late ImageControllers controllers;

  @override
  void initState() {
    controllers = new ImageControllers();
    controllers.clearDataImg();
    controllers.getDetailImage(widget.no_wp);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("GAMBAR SIDAK"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  GetBuilder<ImageControllers>(
                    init: controllers,
                    builder: (params){
                      if(!params.loadingProses){
                        if(params.data_img_all.length > 0){
                          return Container(
                              child: Column(
                                children: <Widget>[
                                  ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: params.data_img_all.length,
                                    physics: ClampingScrollPhysics(),
                                    itemBuilder: (context, item){
                                      // return Container(
                                      //     child:Text(params.data_img[item])
                                      // );
                                      var data = params.data_img_all[item];
                                      return Container(
                                          margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                child: Center(
                                                    child: Image
                                                        .memory(data.encoddecodeBase64Image)
                                                ),
                                              ),
                                            ],
                                          )
                                      );
                                    },
                                  ),
                                ],
                              )
                          );
                        }
                      }

                      if(params.loadingProses){
                        return Container(
                          margin: EdgeInsets.only(top: 120),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }

                      return controllers.widgetImgPick(context);
                    },
                  )
                ],
              )
          )
      ),
    );
  }
}
