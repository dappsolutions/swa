import 'package:flutter/material.dart';
import 'package:swa/config/url.config.dart';
import 'package:swa/ui/Uicolor.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class DocumentSidakViews extends StatefulWidget {
  // const DocumentSidakViews({ Key? key }) : super(key: key);
  String no_wp;
  DocumentSidakViews({required this.no_wp});

  @override
  _DocumentSidakViewsState createState() => _DocumentSidakViewsState();
}

class _DocumentSidakViewsState extends State<DocumentSidakViews> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DOKUMEN SIDAK"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
        child: SfPdfViewer.network(
          "${UrlConfig.BASE_URL_PATH}files/berkas/dokumen_sidak/FORMULIRSWA-${widget.no_wp}.pdf",
          key: _pdfViewerKey,
        ),
      ),
    );
  }
}