import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:signature/signature.dart';
import 'package:swa/app/modules/sidak/controllers/sidak.controllers.dart';
import 'package:swa/app/modules/sidak/views/ambilgambar.views.dart';
import 'package:swa/ui/Uicolor.dart';

class SidakViews extends StatefulWidget {
  String no_wp;
  double lat;
  double lng;
  String lokasi_pekerjaan;
  String uraian_pekerjaan;
  String pengawas_k3;
  String nama_vendor;

  SidakViews(
      {required this.no_wp,
      required this.lat,
      required this.lng,
      required this.lokasi_pekerjaan,
      required this.uraian_pekerjaan,
      required this.pengawas_k3,
      required this.nama_vendor});

  @override
  _SidakViewsState createState() => _SidakViewsState();
}

class _SidakViewsState extends State<SidakViews> {
  late SidakControllers controllers;
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  final SignatureController _controller2 = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  @override
  void initState() {
    controllers = new SidakControllers();
    controllers.getUserIdSession();

    if (widget.no_wp == 'SWA MANDIRI') {
      widget.lokasi_pekerjaan = '-';
      widget.uraian_pekerjaan = '-';
    }
    controllers.setInitValueForm(widget.lokasi_pekerjaan,
        widget.uraian_pekerjaan, widget.pengawas_k3, widget.nama_vendor);
    // _controller.addListener(() => print('Value changed'));
    // _controller2.addListener(() => print('Value changed'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("DETAIL SIDAK"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
              child: GetBuilder<SidakControllers>(
        init: controllers,
        builder: (params) {
          if (params.loadingProses) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          return Column(
            children: <Widget>[
              Container(
                color: Uicolor.hexToColor(Uicolor.grey_young),
                padding: EdgeInsets.all(16),
                child: Center(
                  child: Text(
                    "DATA",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 24),
                alignment: Alignment.topLeft,
                child: Text("Nama Pemegang",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        initialValue: controllers.nama_pemegang,
                        autocorrect: true,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Nama Pemegang SWA',
                          prefixIcon: Icon(
                            Icons.person,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.nama_pemegang = value.toString();
                          controllers.setSwaData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                alignment: Alignment.topLeft,
                child: Text("Email Pemegang",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        initialValue: controllers.email_pemegang,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Email Pemegang SWA',
                          prefixIcon: Icon(
                            Icons.mail_outline,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.email_pemegang = value.toString();
                          controllers.setSwaData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                alignment: Alignment.topLeft,
                child: Text("Lokasi Pekerjaan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        initialValue: widget.no_wp == 'SWA MANDIRI'
                            ? '-'
                            : controllers.lokasi_pekerjaan,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Lokasi Pekerjaan',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {
                          controllers.lokasi_pekerjaan = value.toString();
                          controllers.setSwaData();
                        },
                        onChanged: (value) {
                          controllers.lokasi_pekerjaan = value.toString();
                          controllers.setSwaData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                alignment: Alignment.topLeft,
                child: Text("Uraian Pekerjaan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        initialValue: widget.no_wp == 'SWA MANDIRI'
                            ? '-'
                            : controllers.uraian_pekerjaan,
                        // controller: _controllerDua,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Uraian Pekerjaan',
                          prefixIcon: Icon(
                            Icons.work,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.uraian_pekerjaan = value.toString();
                          controllers.setSwaData();
                        },
                        onTap: () async {},
                      ))),

              Container(
                color: Uicolor.hexToColor(Uicolor.grey_young),
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.only(top: 16),
                child: Center(
                  child: Text(
                    "KELENGKAPAN",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Working Permit",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_WP) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          print("TES WP");
                          controllers.setChecked('wp');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("SOP/IK",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_SOP) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('sop');
                        },
                      ),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Job Safety Analisis",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_JSA) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('jsa');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          "Pengawas K3 & Pengawas Pekerjaan yang \nBerkompeten",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_PENGAWASK3) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('pengawas_k3');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Pekerja Memiliki Sertifikat Kompetensi",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_PEKERJA_SER) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('sertifikat_kom');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          "Peralatan Kerja yang Layak dan \nSesuai dengan Jenis Pekerjaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_PERALATAN) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('peralatan');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Rambu - rambu K3 dan LOTO",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_RAMBUK3) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('rambu_k3');
                        },
                      )
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          "Alat Pelindung Diri (APD) yang Layak dan \nTersedia",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                      GestureDetector(
                        child: GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.CHECKED_APD) {
                              return Icon(
                                Icons.check_circle,
                                color: Uicolor.hexToColor(Uicolor.green),
                              );
                            }

                            return Icon(
                              Icons.check_circle_outline,
                              color: Uicolor.hexToColor(Uicolor.black_young),
                            );
                          },
                        ),
                        onTap: () {
                          controllers.setChecked('apd');
                        },
                      )
                    ],
                  )),
              Container(
                color: Uicolor.hexToColor(Uicolor.grey_young),
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.only(top: 16),
                child: Center(
                  child: Text(
                    "PEKERJAAN",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          "Pelaksanaan Pekerjaan dihentikan sementara \npada jam",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
                    Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: controllers.jam_pelaksanaan_kerja,
                          prefixIcon: Icon(
                            Icons.watch_later,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.jam_pelaksanaan_kerja =
                              value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {
                          DatePicker.showDateTimePicker(context,
                              showTitleActions: true,
                              // minTime: DateTime(2018, 3, 5),
                              // maxTime: DateTime(2019, 6, 7),
                              theme: DatePickerTheme(
                                  headerColor: Colors.orange,
                                  backgroundColor:
                                      Uicolor.hexToColor(Uicolor.white_grey),
                                  itemStyle: TextStyle(
                                      color: Uicolor.hexToColor(
                                          Uicolor.black_young),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                  doneStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16)), onChanged: (date) {
                            // print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                          }, onConfirm: (date) {
                            // print('confirm $date');
                            controllers.jam_pelaksanaan_kerja = date.toString();
                            controllers.setPekerjaanData();
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.id);
                        },
                      ))),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0.3,
              //         child: TextFormField(
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: controllers.jam_pelaksanaan_kerja,
              //             prefixIcon: Icon(
              //               Icons.watch_later,
              //               color: Uicolor.hexToColor(Uicolor.green),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             enabled: false,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.jam_pelaksanaan_kerja = value.toString();
              //             controllers.setPekerjaanData();
              //           },
              //           onTap: ()  {
              //             DatePicker.showDateTimePicker(context,
              //                 showTitleActions: true,
              //                 // minTime: DateTime(2018, 3, 5),
              //                 // maxTime: DateTime(2019, 6, 7),
              //                 theme: DatePickerTheme(
              //                     headerColor: Colors.orange,
              //                     backgroundColor:
              //                         Uicolor.hexToColor(Uicolor.white_grey),
              //                     itemStyle: TextStyle(
              //                         color: Uicolor.hexToColor(
              //                             Uicolor.black_young),
              //                         fontWeight: FontWeight.bold,
              //                         fontSize: 18),
              //                     doneStyle: TextStyle(
              //                         color: Colors.white,
              //                         fontSize: 16)), onChanged: (date) {
              //               // print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
              //             }, onConfirm: (date) {
              //               // print('confirm $date');
              //               controllers.jam_pelaksanaan_kerja = date.toString();
              //               controllers.setPekerjaanData();
              //             },
              //                 currentTime: DateTime.now(),
              //                 locale: LocaleType.id);
              //           },
              //         ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Pekerjaan Dilanjutkan Kembali pada jam",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: controllers.jam_lanjut_pelaksanaan_kerja,
                          prefixIcon: Icon(
                            Icons.watch_later,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.jam_lanjut_pelaksanaan_kerja =
                              value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {
                          DatePicker.showDateTimePicker(context,
                              showTitleActions: true,
                              // minTime: DateTime(2018, 3, 5),
                              // maxTime: DateTime(2019, 6, 7),
                              theme: DatePickerTheme(
                                  headerColor: Colors.orange,
                                  backgroundColor:
                                      Uicolor.hexToColor(Uicolor.white_grey),
                                  itemStyle: TextStyle(
                                      color: Uicolor.hexToColor(
                                          Uicolor.black_young),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                  doneStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16)), onChanged: (date) {
                            // print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                          }, onConfirm: (date) {
                            // print('confirm $date');
                            controllers.jam_lanjut_pelaksanaan_kerja =
                                date.toString();
                            controllers.setPekerjaanData();
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.id);
                        },
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Keterangan Penghentian Pekerjaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        // maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Keterangan',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onChanged: (value) {
                          controllers.ket_dihentikan_kerja = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Temuan 1",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        // maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Temuan 1',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.temuan_1 = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),

              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Jenis Temuan 1",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.2,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0.3,
              //         child: TextFormField(
              //           autocorrect: true,
              //           keyboardType: TextInputType.multiline,
              //           maxLines: null,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Jenis Temuan 1',
              //             prefixIcon: Icon(Icons.bookmark_border, color: Uicolor.hexToColor(Uicolor.green),),
              //             hintStyle: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius: BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white, width: 1),
              //
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius: BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async{
              //
              //           },
              //           onChanged: (value){
              //             controllers.jenis_temuan_1 = value.toString();
              //             controllers.setPekerjaanData();
              //           },
              //           onTap: () async {
              //
              //           },
              //         )
              //     )
              // ),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.jenis_status_temuan_1 == "UC") {
                              return RaisedButton(
                                color: Colors.green,
                                child: Text("UNSAFE CONDITION",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () {
                                  controllers.setUnsafeCondition(0, 1);
                                },
                              );
                            }

                            return RaisedButton(
                              color: Colors.white,
                              child: Text("UNSAFE CONDITION"),
                              onPressed: () {
                                controllers.setUnsafeCondition(0, 1);
                              },
                            );
                          }),
                      GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.jenis_status_temuan_1 == "UA") {
                              return RaisedButton(
                                color: Colors.green,
                                child: Text("UNSAFE ACTION",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () {
                                  controllers.setUnsafeCondition(1, 1);
                                },
                              );
                            }

                            return RaisedButton(
                              color: Colors.white,
                              child: Text("UNSAFE ACTION"),
                              onPressed: () {
                                controllers.setUnsafeCondition(1, 1);
                              },
                            );
                          }),
                    ],
                  )),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Temuan 2",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        // maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Temuan 2',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.temuan_2 = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Jenis Temuan 2",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.2,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0.3,
              //         child: TextFormField(
              //           autocorrect: true,
              //           keyboardType: TextInputType.multiline,
              //           maxLines: null,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Jenis Temuan 2',
              //             prefixIcon: Icon(Icons.bookmark_border, color: Uicolor.hexToColor(Uicolor.green),),
              //             hintStyle: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius: BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white, width: 1),
              //
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius: BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async{
              //
              //           },
              //           onChanged: (value){
              //             controllers.jenis_temuan_2 = value.toString();
              //             controllers.setPekerjaanData();
              //           },
              //           onTap: () async {
              //
              //           },
              //         )
              //     )
              // ),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.jenis_status_temuan_2 == "UC") {
                              return RaisedButton(
                                color: Colors.green,
                                child: Text("UNSAFE CONDITION",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () {
                                  controllers.setUnsafeCondition(0, 2);
                                },
                              );
                            }

                            return RaisedButton(
                              color: Colors.white,
                              child: Text(
                                "UNSAFE CONDITION",
                              ),
                              onPressed: () {
                                controllers.setUnsafeCondition(0, 2);
                              },
                            );
                          }),
                      GetBuilder<SidakControllers>(
                          init: controllers,
                          builder: (params) {
                            if (params.jenis_status_temuan_2 == "UA") {
                              return RaisedButton(
                                color: Colors.green,
                                child: Text("UNSAFE ACTION",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () {
                                  controllers.setUnsafeCondition(1, 2);
                                },
                              );
                            }

                            return RaisedButton(
                              color: Colors.white,
                              child: Text("UNSAFE ACTION"),
                              onPressed: () {
                                controllers.setUnsafeCondition(1, 2);
                              },
                            );
                          }),
                    ],
                  )),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Rekomendasi",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        // maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Rekomendasi',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.rekomendasi = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Keterangan Melanjutkan Pekerjaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        // maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Keterangan',
                          prefixIcon: Icon(
                            Icons.bookmark_border,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.ket_melanjutkan_pekerjaan =
                              value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Nama Pengawas K3 / Pengawas Pekerjaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        initialValue: controllers.nama_pengawas_k3,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Nama',
                          prefixIcon: Icon(
                            Icons.person,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.nama_pengawas_k3 = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Nama PT/Perusahaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0.3,
                      child: TextFormField(
                        autocorrect: true,
                        initialValue: controllers.nama_perusahaan,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Perusahaan',
                          prefixIcon: Icon(
                            Icons.account_balance,
                            color: Uicolor.hexToColor(Uicolor.green),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.nama_perusahaan = value.toString();
                          controllers.setPekerjaanData();
                        },
                        onTap: () async {},
                      ))),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Tanda Tangan Pengawas K3 / Pengawas Pekerjaan",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              controllers.widgetImgSignature(context, _controller),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 3),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    color: Colors.white,
                    child: Text("UNDO"),
                    onPressed: () {
                      _controller.clear();
                    },
                  )),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Tanda Tangan Pemegang SWA",
                          style: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.black_young),
                              fontSize: 14)),
                    ],
                  )),
              controllers.widgetImgSignature(context, _controller2),
              Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 3),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    color: Colors.white,
                    child: Text("UNDO"),
                    onPressed: () {
                      _controller2.clear();
                    },
                  )),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SIMPAN DATA",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    var data = await _controller.toPngBytes();
                    if (data == null) {
                      Get.snackbar(
                          "Informasi", "Tanda Tangan Pengawas K3 Wajib Diisi",
                          backgroundColor: Colors.redAccent);
                      return;
                    }
                    var data64ImgK3 = base64.encode(data);
                    var data2 = await _controller2.toPngBytes();
                    if (data2 == null) {
                      Get.snackbar(
                          "Informasi", "Tanda Tangan Pemegang Wajib Diisi",
                          backgroundColor: Colors.redAccent);
                      return;
                    }
                    var data64ImgSwa = base64.encode(data2);

                    controllers.img_ttd_k3 = data64ImgK3;
                    controllers.img_ttd_swa = data64ImgSwa;
                    controllers.setPekerjaanData();

                    int simpanAll = await controllers.simpanAll(widget.no_wp, widget.lat, widget.lng);
                    if (simpanAll == 1) {
                      String result = await Get.to(AmbilGambarViews(
                        no_wp: widget.no_wp,
                        data_apd: controllers.dataApd,
                        data_safety: controllers.dataSwa,
                        data_pekerjaan: controllers.dataPekerjaa,
                        lat: widget.lat,
                        lng: widget.lng,
                        no_transaksi: controllers.no_transaksi,
                      ));
                      result == "1"
                          ? Get.snackbar("Informasi", "Simpan Sukses",
                              backgroundColor: Colors.green,
                              colorText: Colors.white)
                          : Get.snackbar("Informasi", "Simpan Gagal",
                              backgroundColor: Colors.red,
                              colorText: Colors.white);
                      await Future.delayed(Duration(seconds: 2));
                      Navigator.pop(context);
                    }
                  },
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ],
          );
        },
      ))),
    );
  }
}
