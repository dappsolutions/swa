import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/sidak/controllers/sidak.controllers.dart';
import 'package:swa/app/modules/sidak/views/detailimage.views.dart';
import 'package:swa/app/modules/sidak/views/documentsidak.views.dart';
// import 'package:swa/app/modules/sidak/views/documentsidak.views.dart2';
// import 'package:swa/app/modules/sidak/views/detailimage.views.dart';
import 'package:swa/ui/Uicolor.dart';

class SidakDetailViews extends StatefulWidget {
  String no_wp;
  SidakDetailViews({required this.no_wp});


  @override
  _SidakDetailViewsState createState() => _SidakDetailViewsState();
}

class _SidakDetailViewsState extends State<SidakDetailViews> {
  late SidakControllers controllers;

  @override
  void initState() {
    controllers = new SidakControllers();
    controllers.getDetailSidak(widget.no_wp);
    controllers.setPekerjaanData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DETAIL SIDAK"),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        // iconTheme: IconThemeData(
        //   color: Uicolor.hexToColor(Uicolor.black)
        // ),
      ),
      body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  color: Uicolor.hexToColor(Uicolor.grey_young),
                  padding: EdgeInsets.all(16),
                  child: Center(
                    child: Text("DATA ", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 20, fontWeight: FontWeight.bold),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 24),
                  alignment: Alignment.topLeft,
                  child: Text("Nama Pemegang ", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataSwa.nama_swa == null ? '-' : params.dataSwa.nama_swa, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Email Pemegang ", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataSwa.email_swa == null ? '-' : params.dataSwa.email_swa, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Lokasi Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataSwa.lokasi_pekerjaan == null ? '-' : params.dataSwa.lokasi_pekerjaan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Uraian Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataSwa.uraian_pekerjaan == null ? '-' : params.dataSwa.uraian_pekerjaan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  color: Uicolor.hexToColor(Uicolor.grey_young),
                  margin: EdgeInsets.only(top: 8),
                  padding: EdgeInsets.all(16),
                  child: Center(
                    child: Text("KELENGKAPAN", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 20, fontWeight: FontWeight.bold),),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Working Permit", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.wp == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("SOP/IK", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.sop == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Job Safety Analisis", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.jsa == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Pengawas K3 & Pengawas Pekerjaan yang \nBerkompeten", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.pengawas_k3 == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Pekerja Memiliki Sertifikat Kompetensi", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.sertifikat_kom == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Peralatan Kerja yang Layak dan \nSesuai dengan Jenis Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.peralatan == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Rambu - rambu K3 dan LOTO", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.rambu_k3 == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Alat Pelindung Diri (APD) yang Layak dan \nTersedia", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                        GestureDetector(
                          child: GetBuilder<SidakControllers>(
                            init: controllers,
                            builder: (params){
                              if(!params.loadingProses){
                                return params.dataApd.apd == "yes" ? Icon(Icons.check_circle, color: Uicolor.hexToColor(Uicolor.green),) :Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),) ;
                              }

                              return Icon(Icons.check_circle_outline, color: Uicolor.hexToColor(Uicolor.black_young),);
                            },
                          ),
                          onTap: (){

                          },
                        )
                      ],
                    )
                ),
                Container(
                  color: Uicolor.hexToColor(Uicolor.grey_young),
                  margin: EdgeInsets.only(top: 8),
                  padding: EdgeInsets.all(16),
                  child: Center(
                    child: Text("PEKERJAAN", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 20, fontWeight: FontWeight.bold),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Pelaksanaan Pekerjaan dihentikan sementara \npada jam", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.jam_pekerjaan_dihentikan == null ? '-' : params.dataPekerjaa.jam_pekerjaan_dihentikan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Pekerjaan Dilanjutkan Kembali pada jam", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.jam_pekerjaan_dilanjutkan == null ? '-' : params.dataPekerjaa.jam_pekerjaan_dilanjutkan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Keterangan Penghentian Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.ket_pekerjaan_dihentikan == null ? '-' : params.dataPekerjaa.ket_pekerjaan_dihentikan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Temuan 1", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.temuan_1 == null ? '-' : params.dataPekerjaa.temuan_1, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Jenis Temuan 1", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                // Container(
                //     height: MediaQuery.of(context).size.height * 0.08,
                //     // width: 300,
                //     margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                //     alignment: Alignment.topRight,
                //     child: GetBuilder<SidakControllers>(
                //       init: controllers,
                //       builder: (params){
                //         if(!params.loadingProses){
                //           return Text(params.dataPekerjaa.jenis_temuan_1 == null ? '-' : params.dataPekerjaa.jenis_temuan_1, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                //         }
                //
                //         return Text('-', textAlign: TextAlign.right,);
                //       },
                //     )
                // ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GetBuilder<SidakControllers>(
                            init : controllers,
                            builder:(params){
                              if(params.dataPekerjaa.status_temuan_1 == "0"){
                                return RaisedButton(
                                  color: Colors.green,
                                  child: Text("UNSAFE CONDITION", style: TextStyle(color: Colors.white)),
                                  onPressed: (){
                                    //
                                  },
                                );
                              }

                              return RaisedButton(
                                color: Colors.white,
                                child: Text("UNSAFE CONDITION"),
                                onPressed: (){

                                },
                              );
                            }
                        ),

                        GetBuilder<SidakControllers>(
                            init : controllers,
                            builder:(params){
                              if(params.dataPekerjaa.status_temuan_1 == "1"){
                                return RaisedButton(
                                  color: Colors.green,
                                  child: Text("UNSAFE ACTION", style: TextStyle(color: Colors.white)),
                                  onPressed: (){
                                    controllers.setUnsafeCondition(1, 1);
                                  },
                                );
                              }

                              return RaisedButton(
                                color: Colors.white,
                                child: Text("UNSAFE ACTION"),
                                onPressed: (){
                                  controllers.setUnsafeCondition(1, 1);
                                },
                              );
                            }
                        ),
                      ],
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Temuan 2", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.temuan_2 == null ? '-' : params.dataPekerjaa.temuan_2, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Jenis Temuan 2", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                // Container(
                //     height: MediaQuery.of(context).size.height * 0.08,
                //     // width: 300,
                //     margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                //     alignment: Alignment.topRight,
                //     child: GetBuilder<SidakControllers>(
                //       init: controllers,
                //       builder: (params){
                //         if(!params.loadingProses){
                //           return Text(params.dataPekerjaa.jenis_temuan_2 == null ? '-' : params.dataPekerjaa.jenis_temuan_2, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                //         }
                //
                //         return Text('-', textAlign: TextAlign.right,);
                //       },
                //     )
                // ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GetBuilder<SidakControllers>(
                            init : controllers,
                            builder:(params){
                              if(params.dataPekerjaa.status_temuan_2 == "0"){
                                return RaisedButton(
                                  color: Colors.green,
                                  child: Text("UNSAFE CONDITION", style: TextStyle(color: Colors.white)),
                                  onPressed: (){
                                    //
                                  },
                                );
                              }

                              return RaisedButton(
                                color: Colors.white,
                                child: Text("UNSAFE CONDITION"),
                                onPressed: (){

                                },
                              );
                            }
                        ),

                        GetBuilder<SidakControllers>(
                            init : controllers,
                            builder:(params){
                              if(params.dataPekerjaa.status_temuan_2 == "1"){
                                return RaisedButton(
                                  color: Colors.green,
                                  child: Text("UNSAFE ACTION", style: TextStyle(color: Colors.white)),
                                  onPressed: (){
                                    controllers.setUnsafeCondition(1, 1);
                                  },
                                );
                              }

                              return RaisedButton(
                                color: Colors.white,
                                child: Text("UNSAFE ACTION"),
                                onPressed: (){
                                  controllers.setUnsafeCondition(1, 1);
                                },
                              );
                            }
                        ),
                      ],
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Rekomendasi", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.rekomendasi == null ? '-' : params.dataPekerjaa.rekomendasi, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Keterangan Melanjutkan Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                      ],
                    )
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.ket_melanjutkan_pekerjaan == null ? '-' : params.dataPekerjaa.ket_melanjutkan_pekerjaan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Nama Pengawas K3 / Pengawas Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.nama_pengawas_k3 == null ? '-' : params.dataPekerjaa.nama_pengawas_k3, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Nama PT / Perusahaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.topRight,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          return Text(params.dataPekerjaa.nama_perusahaan == null ? '-' : params.dataPekerjaa.nama_perusahaan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Tanda Tangan Pengawas K3 / Pengawas Pekerjaan", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    // height: MediaQuery.of(context).size.height * 0.08,
                    height: 200,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.center,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          // return Text(params.dataPekerjaa.nama_perusahaan == null ? '-' : params.dataPekerjaa.nama_perusahaan, textAlign: TextAlign.right, style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),);
                          if(params.dataPekerjaa.ttd_k3 != null){
                            var dataImgTtd = base64.decode(params.dataPekerjaa.ttd_k3.toString());
                            return Container(
                              child: Center(
                                  child: DottedBorder(
                                    dashPattern: [8,4],
                                    strokeWidth: 2,
                                    color: Colors.grey,
                                    child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: Container(
                                          height: 300,
                                          child: Image.memory(dataImgTtd),
                                          //O
                                        )
                                    ),
                                  )
                              ),
                            );
                          }
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                  alignment: Alignment.topLeft,
                  child: Text("Tanda Tangan Pemegang SWA", style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 14)),
                ),
                Container(
                    height: 200,
                    // width: 300,
                    margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                    alignment: Alignment.center,
                    child: GetBuilder<SidakControllers>(
                      init: controllers,
                      builder: (params){
                        if(!params.loadingProses){
                          if(params.dataPekerjaa.ttd_swa != null){
                            var dataImgTtd = base64.decode(params.dataPekerjaa.ttd_swa.toString());
                            return Container(
                              child: Center(
                                  child: DottedBorder(
                                    dashPattern: [8,4],
                                    strokeWidth: 2,
                                    color: Colors.grey,
                                    child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: Container(
                                          child: Image.memory(dataImgTtd),
                                          //O
                                        )
                                    ),
                                  )
                              ),
                            );
                          }
                        }

                        return Text('-', textAlign: TextAlign.right,);
                      },
                    )
                ),

                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                  height: MediaQuery.of(context).size.height * 0.07,
                  width: double.infinity,
                  // height: 50,
                  child: RaisedButton(
                    child: Text("DETAIL GAMBAR", style: TextStyle(color: Colors.white),),
                    color: Colors.orangeAccent,
                    onPressed: (){
                      Get.to(DetailImageViews(
                        no_wp: widget.no_wp,
                      ));
                    },
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ],
            ),
          )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.assignment),
        backgroundColor: Colors.orangeAccent,
        onPressed: (){
          Get.to(DocumentSidakViews(
            no_wp: widget.no_wp,
          ));
        },
      ),
    );
  }
}
