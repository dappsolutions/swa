import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:swa/app/modules/version/models/version.model.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';

class VersionServices{
  static Future<List<VersionModels>> getAppVersion(
      Map<String, dynamic> params, List<VersionModels> dataVersion) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_APPS][Routes.GETVERSION]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataVersion.add(VersionModels.fromJson(val));
      }

      return dataVersion;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    return [];
  }
}