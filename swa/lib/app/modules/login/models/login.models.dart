
class LoginModel{
  late String id;
  late String user;
  late String apps;
  late String createddate;
  late String name;
  late String email;

  LoginModel({required this.id, required this.user, required this.apps, required this.createddate, required this.name, required this.email});

  LoginModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.user = json['user'];
    this.apps = json['apps'];
    this.createddate = json['createddate'];
    this.name = json['name'];
    this.email = json['email'];
  }
}