
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_json_mapper/simple_json_mapper.dart';
import 'package:swa/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:swa/app/modules/login/models/login.models.dart';
import 'package:swa/app/modules/login/storage/login.storage.dart';
import 'dart:convert';

import 'package:swa/app/modules/response/models/message.models.dart';
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';
import 'package:swa/config/session.config.dart';
import 'package:swa/mapper.g.dart' as mapper;

class LoginController extends GetxController{
  bool loadingLogin = false;


  @override
  void onInit() {
    super.onInit();
  }

  void signIn(username, pass, context) async{
    this.loadingLogin = true;
    update();

    mapper.init();
    var dataLogin = JsonMapper.serialize(LoginStorage(
        username: username,
        password:pass
    ));

    Map<String, dynamic> params = {};
    params["data"] = dataLogin;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN]), body: params);

    print("URL ${Uri.parse(Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN])}");
    print("DATA ${request.body}");

    if(request.statusCode == 200){
      this.loadingLogin = false;
      update();

      var data = json.decode(request.body);
      // print("DATA ${data}");
      MessageModel respMessage = MessageModel.fromJson(data);
      if(respMessage.is_valid == "1"){
        LoginModel loginModel = LoginModel.fromJson(data["data"]);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("user_id", loginModel.user);
        prefs.setString("email", loginModel.email);
        prefs.setString("username", loginModel.name);

        SessionConfig.sessionSet(loginModel.user, loginModel.email, loginModel.name);
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.green, colorText: Colors.white);
        Navigator.of(context).pop();
        Get.to(DaftarWpViews());
      }else{
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    }else{
      this.loadingLogin = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
  }
}