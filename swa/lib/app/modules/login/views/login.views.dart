import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:swa/app/modules/daftarwp/views/daftarwp.views.dart';
import 'package:swa/app/modules/login/controllers/login.controllers.dart';
import 'package:swa/config/connectivity.dart';
import 'package:swa/ui/Uicolor.dart';

class LoginViews extends StatefulWidget {
  @override
  _LoginViewsState createState() => _LoginViewsState();
}

class _LoginViewsState extends State<LoginViews> {
  late LoginController controller;
  late String username;
  late String pass;
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }

  @override
  void initState() {
    controller = new LoginController();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Uicolor.hexToColor(Uicolor.white),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Icon(
              Icons.account_circle,
              size: 120,
              color: Uicolor.hexToColor(Uicolor.white_grey),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            child: Center(
                child: Text(
              "Application",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 38,
                  color: Uicolor.hexToColor(Uicolor.black)),
            )),
          ),
          Container(
            child: Center(
                child: Text(
              "Sign In to Continue",
              style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
            )),
          ),
          Container(
              margin: EdgeInsets.only(top: 32),
              padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              height: MediaQuery.of(context).size.height * 0.09,
              child: Material(
                  borderRadius: BorderRadius.circular(3),
                  shadowColor: Colors.grey,
                  elevation: 1,
                  child: TextFormField(
                    autocorrect: true,
                    // controller: _controllerDua,
                    decoration: InputDecoration(
                      // prefixIcon: Icon(Icons.search),
                      hintText: 'Username',
                      prefixIcon: Icon(
                        Icons.mail_outline,
                        color: Uicolor.hexToColor(Uicolor.green),
                      ),
                      hintStyle:
                          TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      filled: true,
                      fillColor: Uicolor.hexToColor(Uicolor.white),
                      //
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                    onFieldSubmitted: (value) async {},
                    onChanged: (value) {
                      this.username = value.toString();
                    },
                    onTap: () async {},
                  ))),
          Container(
              margin: EdgeInsets.only(top: 16),
              padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              height: MediaQuery.of(context).size.height * 0.09,
              child: Material(
                  borderRadius: BorderRadius.circular(3),
                  shadowColor: Colors.grey,
                  elevation: 1,
                  child: TextFormField(
                    autocorrect: true,
                    // controller: _controllerDua,
                    decoration: InputDecoration(
                      // prefixIcon: Icon(Icons.search),
                      hintText: 'Password',
                      prefixIcon: Icon(
                        Icons.lock_outline,
                        color: Uicolor.hexToColor(Uicolor.green),
                      ),
                      hintStyle:
                          TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
                      filled: true,
                      fillColor: Uicolor.hexToColor(Uicolor.white),
                      //
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                    obscureText: true,
                    onFieldSubmitted: (value) async {},
                    onChanged: (value) {
                      this.pass = value.toString();
                    },
                    onTap: () async {},
                  ))),
          Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 16),
              height: MediaQuery.of(context).size.height * 0.07,
              width: double.infinity,
              // height: 50,
              child: GetBuilder<LoginController>(
                  init: controller,
                  builder: (params) {
                    if (!params.loadingLogin) {
                      return RaisedButton(
                        child: Text(
                          "LOGIN",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Uicolor.hexToColor(Uicolor.green),
                        onPressed: () {
                          // Get.to(MyHomePage());
                          controller.signIn(this.username, this.pass, context);
                          // Navigator.of(context).pop();
                          // Get.to(DaftarWpViews());
                        },
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(3.0),
                        ),
                      );
                    }

                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  })),
          Container(
            margin: EdgeInsets.only(top: 38),
            child: Center(
                child: Text(
              "Supported By ",
              style: TextStyle(color: Uicolor.hexToColor(Uicolor.grey)),
            )),
          ),
          Container(
            margin: EdgeInsets.only(top: 3),
            child: Center(
                child: Text(
              "Dappsolutions ",
              style: TextStyle(color: Uicolor.hexToColor(Uicolor.green)),
            )),
          ),
        ],
      ))),
    );
  }
}
