
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swa/app/modules/grafik/models/grafik.models.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:swa/config/api.config.dart';
import 'package:swa/config/modules.config.dart';
import 'package:swa/config/routes.config.dart';
import 'package:swa/config/session.config.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GrafikControllers extends GetxController{
  List<charts.Series<dynamic, String>> seriesList = [];
  bool isloading = false;
  List<GrafikModel> data = [];


  Future<void> getUserIdSession() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.user_email = pref.getString("email")!;
    SessionConfig.user_name = pref.getString("username")!;
    this.update();
  }

  Future<void> getGrafikSisak(String year, String month, String upt) async{
    this.isloading = true;
    this.update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["year"] = year;
    params["month"] = month;
    params["upt"] = upt;

    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_GRAFIK][Routes.GETDATA]), body: params);

    // print("DATA WP LIST ${request.body}");
    if(request.statusCode == 200){
      this.isloading = false;
      update();

      var dataGrafik = json.decode(request.body);
      for(var val in dataGrafik["data"]){
        this.data.add(GrafikModel.fromJson(val));
      }


    }else{
      this.isloading = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    List<charts.Series<GrafikModel, String>> datagrafik = [];
    datagrafik = [
      new charts.Series<GrafikModel, String>(
        id: 'Sidak',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (GrafikModel sales, _) => sales.month,
        measureFn: (GrafikModel sales, _) => int.parse(sales.total),
        data: this.data,
      )
    ];

    this.isloading = false;
    this.seriesList = datagrafik;
    this.update();
  }
}