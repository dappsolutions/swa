/// Bar chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swa/app/modules/grafik/controllers/grafik.controllers.dart';
import 'package:swa/ui/Uicolor.dart';

class GrafikViews extends StatefulWidget {
  String tahun;
  String month;
  String upt;
  GrafikViews({required this.tahun, required this.month, required this.upt});

  @override
  _GrafikViewsState createState() => _GrafikViewsState();
}

class _GrafikViewsState extends State<GrafikViews> {
  late List<charts.Series<dynamic, String>> seriesList;
  late bool animate;
  late GrafikControllers controllers;

  @override
  void initState() {
    controllers = new GrafikControllers();
    controllers.getGrafikSisak(widget.tahun, widget.month, widget.upt);
    this.seriesList = _createSampleData();
    this.animate = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("GRAFIK ${widget.tahun}", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                color: Uicolor.hexToColor(Uicolor.grey_young),
                padding: EdgeInsets.all(16),
                child: Center(
                  child: Text(widget.month, style: TextStyle(color: Uicolor.hexToColor(Uicolor.black_young), fontSize: 20, fontWeight: FontWeight.bold),),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(16),
                  // child: new charts.BarChart(
                  //   this.seriesList,
                  //   animate: animate,
                  // )
                  child: GetBuilder<GrafikControllers>(
                    init: controllers,
                    builder: (params){
                      if(!params.isloading){
                        // return Container();
                        return new charts.BarChart(
                          params.seriesList,
                          animate: animate,
                        );
                      }

                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  )
              )
            ],
          ),
        )
      )
    );
  }

  static List<charts.Series<GrafikSidak, String>> _createSampleData() {
    List<GrafikSidak> data = [];
    data.add(GrafikSidak("Jan", 30));
    data.add(GrafikSidak("Feb", 20));

    return [
      new charts.Series<GrafikSidak, String>(
        id: 'Sidak',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (GrafikSidak sales, _) => sales.month,
        measureFn: (GrafikSidak sales, _) => sales.jumlah,
        data: data,
      )
    ];
  }
}




/// Sample ordinal data type.
class GrafikSidak {
  final String month;
  final int jumlah;

  GrafikSidak(this.month, this.jumlah);
}