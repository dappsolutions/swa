


class GrafikModel {
  late String month;
  late String total;
  GrafikModel({required this.month, required this.total});


  GrafikModel.fromJson(Map<String, dynamic> json){
    this.month = json['month'];
    this.total = json['total'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['month'] = this.month;
    data['total'] = this.total;
    return data;
  }
}